import "package:flutter/material.dart";

List<InProgressTaskDetails> inprogresstasksList = [
  InProgressTaskDetails(
      contImgColor: const Color.fromARGB(255, 95, 204, 234),
      taskTitle: "Room 303 Set up",
      taskDescription: "Housekeeping",
      taskDate: "14 July 2024",
      taskRisk: "High",
      taskTiming: "5:00 pm",
      taskWorker: "Garima Bhatia",
      taskStarted: true,
      taskProgressStatus: 30,
      taskProgressStatusColor: Colors.pink),
  InProgressTaskDetails(
      contImgColor: const Color.fromARGB(255, 184, 147, 236),
      taskTitle: "Fire Place check up",
      taskDescription: "Maintainence &..",
      taskDate: "16 July 2024",
      taskRisk: "Low",
      taskTiming: "4:00 pm",
      taskWorker: "Raghu Kumar",
      taskStarted: true,
      taskProgressStatus: 35,
      taskProgressStatusColor: Colors.purpleAccent),
  InProgressTaskDetails(
      contImgColor: const Color.fromARGB(255, 147, 236, 147),
      taskTitle: "Cleaning office",
      taskDescription: "Maintainence &..",
      taskDate: "20 July 2024",
      taskRisk: "High",
      taskTiming: "12:00 pm",
      taskWorker: "Nikhil Rao",
      taskStarted: true,
      taskProgressStatus: 50,
      taskProgressStatusColor: Colors.orange),
  InProgressTaskDetails(
      contImgColor: const Color.fromARGB(255, 184, 147, 236),
      taskTitle: "Office Area",
      taskDescription: "Maintainence &..",
      taskDate: "20 July 2024",
      taskRisk: "High",
      taskTiming: "12:00 pm",
      taskWorker: "Anil Gupta",
      taskStarted: true,
      taskProgressStatus: 30,
      taskProgressStatusColor: Colors.blue),
];

class InProgressTaskDetails {
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskDate;
  String taskRisk;
  String taskTiming;
  String taskWorker;
  bool taskStarted;
  double taskProgressStatus;
  Color taskProgressStatusColor;

  InProgressTaskDetails({
    required this.contImgColor,
    required this.taskTitle,
    required this.taskDescription,
    required this.taskDate,
    required this.taskRisk,
    required this.taskTiming,
    required this.taskWorker,
    required this.taskStarted,
    required this.taskProgressStatusColor,
    required this.taskProgressStatus,
  });
}
