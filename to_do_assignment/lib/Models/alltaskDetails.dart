import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TaskDetailView extends StatelessWidget {
  double height = 210;
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskWorker;
  double taskProgressStatus;
  String taskDate;
  String taskTiming;
  String taskRisk;
  Color taskProgressStatusColor;
  bool taskCompleted;
  bool taskInfo;
  bool taskStarted;
  bool taskOverdue;

  TaskDetailView(
      {super.key,
      required this.taskInfo,
      required this.taskStarted,
      required this.taskCompleted,
      required this.taskOverdue,
      required this.contImgColor,
      required this.taskDate,
      required this.taskTiming,
      required this.taskRisk,
      required this.taskTitle,
      required this.taskWorker,
      required this.taskDescription,
      required this.taskProgressStatus,
      required this.taskProgressStatusColor});

  @override
  Widget build(BuildContext context) {
    return Container();
    // return Padding(
    //   padding: const EdgeInsets.only(bottom: 10.0),
    //   child: Container(
    //     height: height,
    //     decoration: const BoxDecoration(
    //         color: Color.fromARGB(255, 255, 255, 255),
    //         borderRadius: BorderRadius.all(Radius.circular(20)),
    //         boxShadow: [
    //           BoxShadow(
    //             color: Color.fromARGB(75, 158, 158, 158),
    //             offset: Offset(0, 3),
    //             blurRadius: 5,
    //             spreadRadius: 3,
    //           ),
    //         ]),
    //     child: Column(
    //       children: [
    //         Padding(
    //           padding: const EdgeInsets.only(left: 10.0, top: 10),
    //           child: Row(
    //             children: [
    //               Container(
    //                 height: 50,
    //                 width: 50,
    //                 decoration: BoxDecoration(
    //                     color: contImgColor,
    //                     borderRadius: BorderRadius.circular(10)),
    //                 child: const Icon(
    //                   Icons.add,
    //                   color: Colors.white,
    //                 ),
    //               ),
    //               const SizedBox(
    //                 width: 5,
    //               ),
    //               Column(
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: [
    //                   Row(
    //                     mainAxisAlignment: MainAxisAlignment.start,
    //                     children: [
    //                       SizedBox(
    //                         width: 200,
    //                         child: Text(
    //                           taskTitle,
    //                           style: const TextStyle(
    //                               fontWeight: FontWeight.bold, fontSize: 18),
    //                         ),
    //                       ),
    //                       //const Spacer(),
    //                       const SizedBox(
    //                         width: 50,
    //                       ),
    //                       GestureDetector(
    //                         onTap: () {},
    //                         child: const Text(
    //                           "View more",
    //                           style: TextStyle(
    //                             color: Color.fromARGB(255, 1, 158, 48),
    //                             fontSize: 12,
    //                           ),
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                   Row(
    //                     children: [
    //                       Text(
    //                         taskDescription,
    //                         style: const TextStyle(
    //                             fontWeight: FontWeight.normal, fontSize: 13),
    //                       ),
    //                       //const Spacer(),
    //                       const SizedBox(
    //                         width: 50,
    //                       ),
    //                       Container(
    //                         height: 25,
    //                         width: 55,
    //                         alignment: Alignment.center,
    //                         decoration: BoxDecoration(
    //                           borderRadius: BorderRadius.circular(10),
    //                           color: taskRisk == "High"
    //                               ? const Color.fromARGB(33, 255, 86, 74)
    //                               : const Color.fromARGB(40, 189, 238, 191),
    //                         ),
    //                         child: Row(
    //                           mainAxisAlignment: MainAxisAlignment.center,
    //                           children: [
    //                             Icon(
    //                               Icons.bar_chart_rounded,
    //                               size: 15,
    //                               color: taskRisk == "High"
    //                                   ? const Color.fromARGB(255, 245, 61, 47)
    //                                   : const Color.fromARGB(255, 68, 193, 72),
    //                             ),
    //                             Text(
    //                               taskRisk,
    //                               style: TextStyle(
    //                                 fontWeight: FontWeight.normal,
    //                                 fontSize: 12,
    //                                 color: taskRisk == "High"
    //                                     ? const Color.fromARGB(255, 245, 61, 47)
    //                                     : const Color.fromARGB(
    //                                         255, 68, 193, 72),
    //                               ),
    //                             ),
    //                           ],
    //                         ),
    //                       ),
    //                       const SizedBox(
    //                         width: 20,
    //                       ),
    //                       Text(
    //                         taskWorker,
    //                         style: const TextStyle(
    //                             fontWeight: FontWeight.normal, fontSize: 12),
    //                       ),
    //                     ],
    //                   ),
    //                 ],
    //               ),
    //             ],
    //           ),
    //         ),
    //         const Divider(
    //           color: Color.fromARGB(110, 185, 185, 185),
    //         ),
    //         Padding(
    //           padding: const EdgeInsets.symmetric(horizontal: 10.0),
    //           child: Row(
    //             children: [
    //               const Text(
    //                 "Progress Status:  ",
    //                 style: TextStyle(fontSize: 12),
    //               ),
    //               taskprogress(),
    //               const Spacer(),
    //               Text(
    //                 taskDate,
    //                 style: const TextStyle(fontSize: 12),
    //               ),
    //               const SizedBox(
    //                 width: 10,
    //               ),
    //               Text(
    //                 taskTiming,
    //                 style: const TextStyle(fontSize: 12),
    //               ),
    //             ],
    //           ),
    //         ),
    //         checkTaskDetails(),
    //         Padding(
    //           padding: const EdgeInsets.only(left: 12.0),
    //           child: Row(
    //             children: [
    //               Text(checktaskProgress1(),
    //                   style: TextStyle(
    //                     fontSize: 11,
    //                     fontWeight: FontWeight.normal,
    //                     color: checktaskProgressColor(),
    //                   )),
    //             ],
    //           ),
    //         ),
    //         Padding(
    //           padding: const EdgeInsets.symmetric(horizontal: 20.0),
    //           child: Row(
    //             children: [
    //               const Spacer(),
    //               GestureDetector(
    //                 onTap: () {},
    //                 child: Container(
    //                   height: 35,
    //                   width: 90,
    //                   alignment: Alignment.center,
    //                   decoration: BoxDecoration(
    //                     border: Border.all(width: 1, color: setBorderColor()),
    //                     color: setColor(),
    //                     borderRadius: BorderRadius.circular(20),
    //                   ),
    //                   child: checktaskProgressDone(),
    //                 ),
    //               ),
    //               const SizedBox(
    //                 width: 15,
    //               ),
    //               GestureDetector(
    //                 onTap: () {},
    //                 child: taskIsCompleted(),
    //               ),
    //             ],
    //           ),
    //         )
    //       ],
    //     ),
    //   ),
    // );
  }
}
