import "package:flutter/material.dart";

List<OverDueTaskDetails> overduetasksList = [
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskDate: "14 July 2024",
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskOverdue: true,
    taskCompleted: true,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Fire Place check up",
    taskDescription: "Maintainence &..",
    taskDate: "16 July 2024",
    taskRisk: "Low",
    taskTiming: "4:00 pm",
    taskWorker: "Raghu Kumar",
    taskOverdue: true,
    taskCompleted: false,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Cleaning office",
    taskDescription: "Maintainence &..",
    taskDate: "20 July 2024",
    taskRisk: "High",
    taskTiming: "12:00 pm",
    taskWorker: "Nikhil Rao",
    taskOverdue: true,
    taskCompleted: false,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Office Area",
    taskDescription: "Maintainence &..",
    taskDate: "20 July 2024",
    taskRisk: "High",
    taskTiming: "12:00 pm",
    taskWorker: "Anil Gupta",
    taskOverdue: true,
    taskCompleted: true,
  ),
];

class OverDueTaskDetails {
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskDate;
  String taskRisk;
  String taskTiming;
  String taskWorker;
  bool taskOverdue;
  bool taskCompleted;

  OverDueTaskDetails({
    required this.contImgColor,
    required this.taskTitle,
    required this.taskOverdue,
    required this.taskDescription,
    required this.taskDate,
    required this.taskRisk,
    required this.taskTiming,
    required this.taskWorker,
    required this.taskCompleted,
  });
}

Widget showOverdueTasks() {
  return ListView.builder(
    itemCount: overduetasksList.length,
    itemBuilder: (context, index) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: Container(
          height: 210,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 255, 255, 255),
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(75, 158, 158, 158),
                  offset: Offset(0, 3),
                  blurRadius: 5,
                  spreadRadius: 3,
                ),
              ]),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0, top: 10),
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          color: overduetasksList[index].contImgColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 200,
                              child: Text(
                                overduetasksList[index].taskTitle,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                            ),
                            //const Spacer(),
                            const SizedBox(
                              width: 50,
                            ),
                            GestureDetector(
                              onTap: () {},
                              child: const Text(
                                "View more",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 1, 158, 48),
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              overduetasksList[index].taskDescription,
                              style: const TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 13),
                            ),
                            //const Spacer(),
                            const SizedBox(
                              width: 50,
                            ),
                            Container(
                              height: 25,
                              width: 55,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: overduetasksList[index].taskRisk ==
                                        "High"
                                    ? const Color.fromARGB(33, 255, 86, 74)
                                    : const Color.fromARGB(40, 189, 238, 191),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.bar_chart_rounded,
                                    size: 15,
                                    color: overduetasksList[index].taskRisk ==
                                            "High"
                                        ? const Color.fromARGB(255, 245, 61, 47)
                                        : const Color.fromARGB(
                                            255, 68, 193, 72),
                                  ),
                                  Text(
                                    overduetasksList[index].taskRisk,
                                    style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12,
                                      color: overduetasksList[index].taskRisk ==
                                              "High"
                                          ? const Color.fromARGB(
                                              255, 245, 61, 47)
                                          : const Color.fromARGB(
                                              255, 68, 193, 72),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Text(
                              overduetasksList[index].taskWorker,
                              style: const TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Divider(
                color: Color.fromARGB(110, 185, 185, 185),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: [
                    const Text(
                      "Progress Status:  ",
                      style: TextStyle(fontSize: 12),
                    ),
                    const Text(
                      "0%",
                      style: TextStyle(color: Colors.red),
                    ),
                    const Spacer(),
                    Text(
                      overduetasksList[index].taskDate,
                      style: const TextStyle(fontSize: 12),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      overduetasksList[index].taskTiming,
                      style: const TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Stack(
                  children: [
                    SizedBox(
                      height: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(children: [
                            Container(
                              height: 2,
                              width: 360,
                              color: Colors.grey,
                            ),
                            Positioned(
                              child: Container(
                                  height: 2, width: 360, color: Colors.red),
                            ),
                          ])
                        ],
                      ),
                    ),
                    Positioned(left: 10, bottom: 3, child: overdue(index)),
                    Positioned(left: 100, bottom: 3, child: overdue(index)),
                    Positioned(left: 190, bottom: 3, child: overdue(index)),
                    Positioned(left: 280, bottom: 3, child: overdue(index)),
                    Positioned(
                      left: 360,
                      bottom: 3,
                      child: overdue(index),
                    )
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Row(
                  children: [
                    Text("Overdue",
                        style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.normal,
                            color: Colors.red)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: [
                    const Spacer(),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        height: 35,
                        width: 90,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(width: 1, color: Colors.grey),
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                height: 22,
                                width: 22,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1.5, color: Colors.white),
                                    shape: BoxShape.circle),
                                child: const Icon(
                                  Icons.check,
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  size: 18,
                                )),
                            const Text(
                              "  Done",
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255)),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1.5, color: Colors.grey),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              shape: BoxShape.rectangle),
                          child: const Icon(
                            Icons.more_horiz_rounded,
                            color: Colors.grey,
                            size: 18,
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    },
  );
}

Widget overdue(int index) {
  return Row(
    children: [
      Container(
          width: 25,
          height: 25,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromARGB(213, 197, 24, 18),
          ),
          child: Icon(
            overduetasksList[index].taskCompleted ? Icons.circle : Icons.check,
            size: 25,
            color: Colors.white,
          )),
    ],
  );
}

bool isOverdue = false;
bool isComplete = false;

Widget emptyTaskProgress(int index) {
  return Row(
    children: [
      Container(
        width: 25,
        height: 25,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromARGB(255, 184, 184, 183),
        ),
        child: overduetasksList[index].taskCompleted
            ? const Icon(Icons.check)
            : const Icon(Icons.circle, color: Colors.white, size: 24),
      ),
    ],
  );
}
