// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

List<CompletedTaskDetails> completedtasksList = [
  CompletedTaskDetails(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskDate: "14 July 2024",
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
  ),
  CompletedTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Fire Place check up",
    taskDescription: "Maintainence &..",
    taskDate: "16 July 2024",
    taskRisk: "Low",
    taskTiming: "4:00 pm",
    taskWorker: "Raghu Bhatia",
  ),
  CompletedTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Cleaning office",
    taskDescription: "Maintainence &..",
    taskDate: "20 July 2024",
    taskRisk: "High",
    taskTiming: "12:00 pm",
    taskWorker: "Nikhil Ingle",
  ),
];

class CompletedTaskDetails {
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskDate;
  String taskRisk;
  String taskTiming;
  String taskWorker;

  CompletedTaskDetails(
      {required this.contImgColor,
      required this.taskTitle,
      required this.taskDescription,
      required this.taskDate,
      required this.taskRisk,
      required this.taskTiming,
      required this.taskWorker});
}

class CreateCategoryContainer extends StatelessWidget {
  Color contColor;
  Color borderColor;
  Color circleColor;
  double height = 40;
  double width = 170;
  String taskCategory;
  int noTask;

  CreateCategoryContainer({
    super.key,
    required this.contColor,
    required this.borderColor,
    required this.taskCategory,
    required this.noTask,
    required this.circleColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: contColor,
          border: Border.all(color: borderColor, width: 1)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: 20,
                width: 20,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: circleColor,
                ),
                child: const Icon(
                  Icons.circle,
                  size: 8,
                  color: Colors.white,
                )),
            const SizedBox(
              width: 15,
            ),
            Text(
              taskCategory,
              style:
                  const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            const Spacer(),
            Text(
              "$noTask",
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
