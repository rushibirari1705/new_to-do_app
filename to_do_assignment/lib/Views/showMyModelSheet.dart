// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

String taskDetails = 'TASK DETAILS';

// List of items in our dropdown menu
List<String> items = [
  'TASK DETAILS',
  'TASK CATEGORY',
  'TASK DESCRIPTION',
];

List<String> checkList = [
  "Door mat & entrance to be cleaned",
  "Fan blades to be cleaned and no noise",
  "working AC with remote",
  "clean ceiling and floor",
  "Working bedside lights",
  "Pillows as per number of guests",
  "Fresh Linen",
  "Clean balcony, furniture & floor"
];

TextEditingController addchecklistController = TextEditingController();

class TaskDetailsClass extends StatefulWidget {
  const TaskDetailsClass({super.key});

  @override
  State<TaskDetailsClass> createState() => _TaskDetailsClassState();
}

class _TaskDetailsClassState extends State<TaskDetailsClass> {
  @override
  Widget build(BuildContext context) {
    return showAddTaskBottomSheet(context);
  }

  Widget showAddTaskBottomSheet(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 0.0),
              child: Column(
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextButton(
                          onPressed: () {},
                          child: const Text(
                            "Cancel",
                            style: TextStyle(fontSize: 15, color: Colors.red),
                          ),
                        ),
                        const Text(
                          "New Task",
                          style: TextStyle(
                              fontSize: 16,
                              color: Color.fromARGB(171, 0, 0, 0),
                              fontWeight: FontWeight.bold),
                        ),
                        TextButton(
                          onPressed: () {},
                          child: const Text(
                            "Save",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.green,
                            ),
                          ),
                        ),
                      ]),
                  const Divider(
                    color: Colors.grey,
                    thickness: 0.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        DropdownButton(
                          value: taskDetails,
                          icon: const Icon(
                            Icons.keyboard_arrow_down,
                            applyTextScaling: true,
                            color: Colors.green,
                            size: 30,
                          ),
                          items: items.map((String items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Text(items),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              taskDetails = newValue!;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: const Row(
                      children: [
                        SizedBox(
                          width: 410,
                          child: ExpansionTile(
                            title: Text("TASK ALLOTMENT"),
                            controlAffinity: ListTileControlAffinity.trailing,
                            children: [
                              Row(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Icon(
                                            Icons.calendar_month_outlined,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                          Text(" SCHEDULE"),
                                          SizedBox(
                                            width: 5,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "28 July 2023 5:30 PM",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.keyboard_arrow_up_outlined,
                                            size: 20,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_on,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                          Text(" LOCATION")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            " Enter Location",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.keyboard_arrow_up_outlined,
                                            size: 20,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.person_sharp,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                          Text(" ASSIGN TO")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            " Enter Assignee",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.keyboard_arrow_up_outlined,
                                            size: 20,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.bar_chart,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                          Text(" PRIORITY")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Medium",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.keyboard_arrow_up_outlined,
                                            size: 20,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(width: 60),
                                  Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            Icons.notifications_active_outlined,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                          Text(" REMINDER")
                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "03 min before",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.keyboard_arrow_up_outlined,
                                            size: 20,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Row(
                          children: [
                            Icon(
                              Icons.star_border_outlined,
                              size: 20,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "POINTS",
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Color.fromARGB(150, 0, 0, 0)),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 22,
                          width: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border:
                                  Border.all(color: Colors.grey, width: 0.5)),
                          child: const Text(
                            "1000",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 15.0, top: 12, bottom: 20),
                    child: Row(
                      children: [
                        Container(
                          height: 55,
                          width: 300,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 255, 255, 255),
                            shape: BoxShape.rectangle,
                            border: Border.all(
                                width: 1,
                                color: Color.fromARGB(97, 158, 158, 158)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          child: TextFormField(
                            controller: addchecklistController,
                            keyboardType: TextInputType.text,
                            decoration: const InputDecoration(
                              contentPadding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              prefixIconColor: Color.fromRGBO(204, 211, 196, 1),
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(164, 164, 164, 1),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300),
                              border: InputBorder.none,
                              hintText: "Type Here & Add Check List",
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  checkList.add(addchecklistController.text);
                                  addchecklistController.clear();
                                });
                              },
                              icon: Icon(Icons.add_circle_outline,
                                  size: 40, color: Colors.green)),
                        )
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 15.0, bottom: 5),
                    child: Row(
                      children: [
                        Text(
                          "Check List",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 250,
                    child: ListView.builder(
                        itemCount: checkList.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(left: 15.0, top: 5),
                            child: Row(
                              children: [
                                Container(
                                  height: 30,
                                  width: 30,
                                  decoration: const BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle),
                                  child: const Icon(
                                    Icons.remove,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SizedBox(
                                  width: 320,
                                  child: Text(
                                    checkList[index],
                                    style: const TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                                Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            width: .5, color: Colors.grey)),
                                    child: Center(
                                      child: const Icon(
                                        Icons.warning_rounded,
                                        size: 20,
                                        color:
                                            Color.fromARGB(142, 122, 122, 122),
                                      ),
                                    )),
                              ],
                            ),
                          );
                        }),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.keyboard_arrow_down_outlined,
                            color: Colors.green,
                          ))
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 12, bottom: 20, right: 15),
                    child: Row(
                      children: [
                        Container(
                          height: 55,
                          width: 380,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 255, 255, 255),
                            shape: BoxShape.rectangle,
                            border: Border.all(
                                width: 1,
                                color: Color.fromARGB(97, 158, 158, 158)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Save this template "),
                              SizedBox(width: 20),
                              ToggleSwitch(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                        height: 55,
                        width: 330,
                        decoration: const BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 20),
                              blurRadius: 40,
                              spreadRadius: 1,
                            ),
                          ],
                        ),
                        alignment: Alignment.center,
                        child: const Text(
                          "Create Task",
                          style: TextStyle(
                            fontSize: 20,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontWeight: FontWeight.w500,
                          ),
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ToggleSwitch extends StatefulWidget {
  const ToggleSwitch({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _ToggleSwitchState createState() => _ToggleSwitchState();
}

class _ToggleSwitchState extends State<ToggleSwitch> {
  bool isSwitched = true;

  void _toggleSwitch(bool value) {
    setState(() {
      isSwitched = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: isSwitched,
      onChanged: _toggleSwitch,
      activeTrackColor: Colors.green,
      activeColor: Colors.white,
    );
  }
}
