import 'package:flutter/material.dart';

import '../Models/completetaskModelClass.dart';
import '../Models/overdueModelClass.dart';
import '../Models/todotaskModelClass.dart';
import 'showCompletedTask.dart';
import 'showInprogressTask.dart';
import 'showMyModelSheet.dart';

bool todotasksection = false;
bool inprogresstasksection = false;
bool overduetasksection = false;
bool completedtasksection = false;

int i = 5;

List<String> monthList = [
  "Jan 2024",
  "Feb 2024",
  "Mar 2024",
  "Apr 2024",
  "May 2024",
  "Jun 2024",
  "July 2024",
  "Aug 2024",
  "Sep 2024",
  "Oct 2024",
  "Nov 2024",
  "Dec 2024"
];

List<TaskDetailView> alltaskdetailsList = [
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskCompleted: false,
    taskInfo: true,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "14 July 2024",
    taskProgressStatus: 30,
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: Colors.amber,
  ),
  TaskDetailView(
    taskCompleted: true,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: false,
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Fire Place check up",
    taskDescription: "Maintainence &..",
    taskDate: "16 July 2024",
    taskProgressStatus: 30,
    taskRisk: "Low",
    taskTiming: "4:00 pm",
    taskWorker: "Raghu Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 214, 110, 222),
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "15 July 2024",
    taskProgressStatus: 40,
    taskRisk: "Low",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 23, 156, 208),
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 402 Set up",
    taskDescription: "Cleaning",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "14 July 2024",
    taskProgressStatus: 40,
    taskRisk: "Low",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 208, 146, 23),
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 402 Set up",
    taskDescription: "Cleaning",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: false,
    taskDate: "14 July 2024",
    taskProgressStatus: 0,
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 208, 146, 23),
  ),
];

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 2.0),
        child: Column(
          children: [
            mainTaskBar(),
            monthSection(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: SizedBox(
                height: 80,
                child: ListView.builder(
                    itemCount: 8,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return dateSection(index);
                    }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: twoSections(),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: SizedBox(
                  height: 90,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = !todotasksection;
                                overduetasksection = false;
                                completedtasksection = false;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "To Do Task",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 214, 3),
                              contColor: todotasksection
                                  ? const Color.fromARGB(209, 251, 248, 212)
                                  : const Color.fromARGB(209, 255, 254, 244),
                              borderColor: todotasksection
                                  ? const Color.fromARGB(255, 237, 214, 3)
                                  : const Color.fromARGB(209, 255, 254, 244),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = false;
                                completedtasksection = false;
                                inprogresstasksection = !inprogresstasksection;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "In-Progress",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 163, 3),
                              contColor: inprogresstasksection
                                  ? const Color.fromARGB(120, 251, 239, 212)
                                  : const Color.fromARGB(120, 254, 245, 240),
                              borderColor: inprogresstasksection
                                  ? const Color.fromARGB(255, 237, 163, 3)
                                  : const Color.fromARGB(120, 254, 245, 240),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = !overduetasksection;
                                completedtasksection = false;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "Overdue",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 85, 3),
                              contColor: overduetasksection
                                  ? const Color.fromARGB(120, 251, 220, 212)
                                  : const Color.fromARGB(120, 255, 241, 238),
                              borderColor: overduetasksection
                                  ? const Color.fromARGB(255, 237, 85, 3)
                                  : const Color.fromARGB(120, 255, 241, 238),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = false;
                                completedtasksection = !completedtasksection;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "Completed",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 24, 128, 1),
                              contColor: completedtasksection
                                  ? const Color.fromARGB(120, 227, 251, 217)
                                  : const Color.fromARGB(120, 238, 251, 232),
                              borderColor: completedtasksection
                                  ? const Color.fromARGB(255, 24, 128, 1)
                                  : const Color.fromARGB(120, 238, 251, 232),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ),
            SizedBox(
              height: 532,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: todotasksection
                    ? showToDoTasks()
                    : inprogresstasksection
                        ? showInProgressTasks()
                        : completedtasksection
                            ? showCompletedTasks()
                            : overduetasksection
                                ? showOverdueTasks()
                                : allTaskDetailView(),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 160.0),
        child: FloatingActionButton(
          onPressed: addTaskBottomSheet,
          child: Container(
            width: 56,
            height: 56,
            decoration: const BoxDecoration(
                color: Colors.pink,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: const Icon(
              Icons.add,
              color: Colors.white,
              size: 30,
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Future addTaskBottomSheet() {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      useRootNavigator: false,
      useSafeArea: true,
      builder: (context) => const SafeArea(
        child: TaskDetailsClass(),
      ),
    );
  }

  Widget allTaskDetailView() {
    setState(() {});
    return ListView.builder(
        itemCount: alltaskdetailsList.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Container(
              height: 210,
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 255, 255, 255),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromARGB(75, 158, 158, 158),
                      offset: Offset(0, 3),
                      blurRadius: 5,
                      spreadRadius: 3,
                    ),
                  ]),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Row(
                      children: [
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              color: alltaskdetailsList[index].contImgColor,
                              borderRadius: BorderRadius.circular(10)),
                          child: const Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 200,
                                  child: Text(
                                    alltaskdetailsList[index].taskTitle,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                                //const Spacer(),
                                const SizedBox(
                                  width: 50,
                                ),
                                GestureDetector(
                                  onTap: () {},
                                  child: const Text(
                                    "View more",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 1, 158, 48),
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  alltaskdetailsList[index].taskDescription,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 13),
                                ),
                                //const Spacer(),
                                const SizedBox(
                                  width: 50,
                                ),
                                Container(
                                  height: 25,
                                  width: 55,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: alltaskdetailsList[index].taskRisk ==
                                            "High"
                                        ? const Color.fromARGB(33, 255, 86, 74)
                                        : const Color.fromARGB(
                                            40, 189, 238, 191),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.bar_chart_rounded,
                                        size: 15,
                                        color: alltaskdetailsList[index]
                                                    .taskRisk ==
                                                "High"
                                            ? const Color.fromARGB(
                                                255, 245, 61, 47)
                                            : const Color.fromARGB(
                                                255, 68, 193, 72),
                                      ),
                                      Text(
                                        alltaskdetailsList[index].taskRisk,
                                        style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 12,
                                          color: alltaskdetailsList[index]
                                                      .taskRisk ==
                                                  "High"
                                              ? const Color.fromARGB(
                                                  255, 245, 61, 47)
                                              : const Color.fromARGB(
                                                  255, 68, 193, 72),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  alltaskdetailsList[index].taskWorker,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const Divider(
                    color: Color.fromARGB(110, 185, 185, 185),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        const Text(
                          "Progress Status:  ",
                          style: TextStyle(fontSize: 12),
                        ),
                        taskprogress(index),
                        const Spacer(),
                        Text(
                          alltaskdetailsList[index].taskDate,
                          style: const TextStyle(fontSize: 12),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          alltaskdetailsList[index].taskTiming,
                          style: const TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  checkTaskDetails(index),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Row(
                      children: [
                        Text(checktaskProgress1(index),
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.normal,
                              color: checktaskProgressColor(index),
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: [
                        const Spacer(),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 35,
                            width: 90,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  width: 1, color: setBorderColor(index)),
                              color: setColor(index),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: checktaskProgressDone(index),
                          ),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: taskIsCompleted(index),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget twoSections() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {},
          child: Container(
            decoration: const BoxDecoration(
                color: Color.fromARGB(74, 248, 209, 223),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            height: 40,
            width: 160,
            alignment: Alignment.center,
            child: const Text(
              "Task Overview",
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.normal,
                  color: Color.fromARGB(221, 234, 54, 114)),
            ),
          ),
        ),
        GestureDetector(
          child: Container(
            decoration: const BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            height: 40,
            width: 160,
            alignment: Alignment.center,
            child: const Text(
              "My SOP's",
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.normal,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget mainTaskBar() {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: Row(
        children: [
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.arrow_back,
              size: 25,
            ),
            color: const Color.fromARGB(255, 1, 158, 48),
          ),
          const SizedBox(
            width: 5,
          ),
          const Text(
            "Manage Tasks",
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 1, 1, 1)),
          ),
          const SizedBox(
            width: 10,
          ),
          const Icon(
            Icons.info_outline,
            size: 25,
            color: Color.fromARGB(255, 1, 158, 48),
          ),
          const SizedBox(
            width: 10,
          ),
          const Spacer(),
          GestureDetector(
            onTap: () {},
            child: Container(
                height: 25,
                width: 120,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Color.fromARGB(255, 222, 15, 0)),
                alignment: Alignment.center,
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.warning,
                      color: Colors.white,
                      size: 15,
                    ),
                    Text(
                      "  Incident Logs",
                      style: TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(255, 255, 255, 1),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                )),
          ),
          const SizedBox(
            width: 5,
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.search,
              size: 30,
            ),
            color: const Color.fromARGB(255, 1, 158, 48),
          ),
        ],
      ),
    );
  }

  Widget monthSection() {
    return Row(
      children: [
        IconButton(
            onPressed: () {
              setState(() {
                if (i > -1) {
                  i--;
                } else {
                  i = 0;
                }
              });
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              size: 20,
              color: Color.fromARGB(255, 1, 158, 48),
            )),
        Text(
          monthList[i],
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        IconButton(
            onPressed: () {
              setState(() {
                setState(() {
                  if (i < monthList.length) {
                    i++;
                  } else {
                    i = 0;
                  }
                });
              });
            },
            icon: const Icon(
              Icons.arrow_forward_ios,
              size: 20,
              color: Color.fromARGB(255, 1, 158, 48),
            )),
        const Spacer(),
        IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.calendar_month_outlined,
              size: 25,
              color: Color.fromARGB(255, 1, 158, 48),
            )),
      ],
    );
  }

  List dateList = [07, 08, 09, 10, 11, 12, 13, 14];
  List<String> dayList = [
    "Fri",
    "Sat",
    "Sun",
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
  ];

  Widget dateSection(int index) {
    bool isClick = true;

    return Padding(
      padding: const EdgeInsets.only(left: 2.0),
      child: GestureDetector(
        onTap: () {
          setState(() {
            isClick = !isClick;
          });
        },
        child: Container(
          height: 20,
          width: 60,
          decoration: BoxDecoration(
            border: Border.all(
                width: 1.5,
                color: index == 0
                    ? const Color.fromARGB(221, 234, 54, 114)
                    : Colors.transparent),
            borderRadius: const BorderRadius.all(Radius.circular(15)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "${dateList[index]}",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: index == 0
                        ? const Color.fromARGB(221, 234, 54, 114)
                        : const Color.fromARGB(230, 32, 32, 32)),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                dayList[index],
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: index == 0
                        ? const Color.fromARGB(221, 234, 54, 114)
                        : const Color.fromARGB(148, 48, 48, 48)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

bool isComplete = true;

// ignore: must_be_immutable
class TaskDetailView extends StatelessWidget {
  double height = 210;
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskWorker;
  double taskProgressStatus;
  String taskDate;
  String taskTiming;
  String taskRisk;
  Color taskProgressStatusColor;
  bool taskCompleted;
  bool taskInfo;
  bool taskStarted;
  bool taskOverdue;

  TaskDetailView(
      {super.key,
      required this.taskInfo,
      required this.taskStarted,
      required this.taskCompleted,
      required this.taskOverdue,
      required this.contImgColor,
      required this.taskDate,
      required this.taskTiming,
      required this.taskRisk,
      required this.taskTitle,
      required this.taskWorker,
      required this.taskDescription,
      required this.taskProgressStatus,
      required this.taskProgressStatusColor});

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

Color setColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.grey;
  } else {
    Colors.green;
  }
  return Colors.transparent;
}

Color setBorderColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.grey;
  } else {
    Colors.green;
  }
  return Colors.green;
}

Widget taskIsCompleted(int index) {
  return alltaskdetailsList[index].taskCompleted
      ? Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              border: Border.all(width: 1.5, color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              shape: BoxShape.rectangle),
          child: const Icon(
            Icons.more_horiz_rounded,
            color: Colors.grey,
            size: 18,
          ))
      : Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              border: Border.all(width: 1.5, color: Colors.green),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              shape: BoxShape.rectangle),
          child: const Icon(
            Icons.more_horiz_rounded,
            color: Colors.green,
            size: 18,
          ));
}

Widget checktaskProgressDone(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            height: 22,
            width: 22,
            decoration: BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white),
                shape: BoxShape.circle),
            child: const Icon(
              Icons.check,
              color: Color.fromARGB(255, 255, 255, 255),
              size: 18,
            )),
        const Text(
          "  Done",
          style: TextStyle(color: Color.fromARGB(255, 255, 255, 255)),
        )
      ],
    );
  } else if (alltaskdetailsList[index].taskStarted) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.play_circle_outline_rounded, color: Colors.green),
        Text(
          "  Start",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  } else {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.check, color: Colors.green),
        Text(
          "  Done",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  }
}

Widget taskprogress(int index) {
  if (alltaskdetailsList[index].taskOverdue) {
    return const Text(
      "Overdue",
      style: TextStyle(color: Colors.red),
    );
  } else if (alltaskdetailsList[index].taskCompleted) {
    return const Text(
      "100%",
      style: TextStyle(color: Colors.green),
    );
  } else {
    return Text(
      "${alltaskdetailsList[index].taskProgressStatus}",
      style:
          TextStyle(color: alltaskdetailsList[index].taskProgressStatusColor),
    );
  }
}

Color? checktaskProgressColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.green;
  } else if (alltaskdetailsList[index].taskOverdue) {
    return Colors.red;
  } else {
    return alltaskdetailsList[index].taskProgressStatusColor;
  }
}

String checktaskProgress1(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return "Completed";
  } else if (alltaskdetailsList[index].taskOverdue) {
    return "Overdue";
  } else {
    return "In Progress";
  }
}

Widget checkTaskDetails(int index) {
  if (alltaskdetailsList[index].taskStarted) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child: Container(
                      height: 2,
                      width: 150,
                      color: alltaskdetailsList[index].taskProgressStatusColor,
                    ),
                  ),
                ])
              ],
            ),
          ),
          Positioned(
            left: 10,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: alltaskdetailsList[index].taskProgressStatusColor,
                  ),
                  child: isComplete
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 15,
                        )
                      : const Icon(Icons.circle, color: Colors.white),
                ),
              ],
            ),
          ),
          Positioned(
            left: 100,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: alltaskdetailsList[index].taskProgressStatusColor,
                  ),
                  child: isComplete
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 15,
                        )
                      : const Icon(Icons.circle, color: Colors.white),
                ),
              ],
            ),
          ),
          Positioned(
            left: 190,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      border: Border.all(
                          color:
                              alltaskdetailsList[index].taskProgressStatusColor,
                          width: 2)),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : Icon(Icons.circle,
                          color:
                              alltaskdetailsList[index].taskProgressStatusColor,
                          size: 10),
                ),
              ],
            ),
          ),
          Positioned(
            left: 280,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      border: Border.all(color: Colors.grey, width: 2)),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : const Icon(Icons.circle,
                          color: Color.fromARGB(170, 120, 120, 120), size: 10),
                ),
              ],
            ),
          ),
          Positioned(
            left: 360,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromARGB(255, 184, 184, 183),
                  ),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : const Icon(Icons.circle, color: Colors.white, size: 24),
                ),
              ],
            ),
          )
        ],
      ),
    );
  } else if (alltaskdetailsList[index].taskCompleted) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child:
                        Container(height: 2, width: 360, color: Colors.green),
                  ),
                ])
              ],
            ),
          ),
          Positioned(left: 10, bottom: 3, child: completed()),
          Positioned(left: 100, bottom: 3, child: completed()),
          Positioned(left: 190, bottom: 3, child: completed()),
          Positioned(left: 280, bottom: 3, child: completed()),
          Positioned(
            left: 360,
            bottom: 3,
            child: completed(),
          )
        ],
      ),
    );
  } else if (alltaskdetailsList[index].taskOverdue) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child: Container(height: 2, width: 360, color: Colors.red),
                  ),
                ])
              ],
            ),
          ),
          Positioned(left: 10, bottom: 3, child: overdue(index)),
          Positioned(left: 100, bottom: 3, child: overdue(index)),
          Positioned(left: 190, bottom: 3, child: overdue(index)),
          Positioned(left: 280, bottom: 3, child: overdue(index)),
          Positioned(
            left: 360,
            bottom: 3,
            child: overdue(index),
          )
        ],
      ),
    );
  } else {
    return Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Stack(
          children: [
            SizedBox(
              height: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(children: [
                    Container(
                      height: 2,
                      width: 360,
                      color: Colors.grey,
                    ),
                    Positioned(
                      child: Container(
                        height: 2,
                        width: 0,
                        color:
                            alltaskdetailsList[index].taskProgressStatusColor,
                      ),
                    ),
                  ])
                ],
              ),
            ),
            Positioned(
              left: 10,
              bottom: 3,
              child: Row(
                children: [
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        border: Border.all(
                            color: alltaskdetailsList[index]
                                .taskProgressStatusColor,
                            width: 2)),
                    child: !isComplete
                        ? const Icon(Icons.check)
                        : Icon(Icons.circle,
                            color: alltaskdetailsList[index]
                                .taskProgressStatusColor,
                            size: 10),
                  ),
                ],
              ),
            ),
            Positioned(left: 100, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 190, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 280, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 360, bottom: 3, child: emptyTaskProgress()),
          ],
        ));
  }
}

Widget emptyTaskProgress() {
  return Row(
    children: [
      Container(
        width: 25,
        height: 25,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromARGB(255, 184, 184, 183),
        ),
        child: !isComplete
            ? const Icon(Icons.check)
            : const Icon(Icons.circle, color: Colors.white, size: 24),
      ),
    ],
  );
}

Widget completed() {
  return Row(
    children: [
      Container(
          width: 25,
          height: 25,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.green,
          ),
          child: const Icon(
            Icons.check,
            size: 15,
            color: Colors.white,
          )),
    ],
  );
}
